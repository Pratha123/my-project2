FROM node:16

# Create directory for application
WORKDIR /usr/src/app

# Install application dependencies
COPY package*.json ./
COPY . .
RUN npm install

EXPOSE  8080
CMD [ "node", "services/email.js" ]
